import requests
# Fossology API base URL and token
Fossology_API_URL = "https://fossology.eclipse.org/repo/api/v1"
Fossology_API_TOKEN = ${vars.FOSSOLOGY_TOKEN}
echo ${Fossology_API_TOKEN}

# Set up headers with the API token
HEADERS = {
    "Authorization": f"Bearer {Fossology_API_TOKEN}",
    "Content-Type": "application/json"
}

def get_folders():
    """
    Retrieve and print folder names from the Fossology REST API.

    Returns:
        None
    """
    url = f"{Fossology_API_URL}/folders"
    response = requests.get(url, headers=HEADERS)

    if response.status_code == 200:
        folders = response.json()
        print("Folders in Fossology:")
        for folder in folders:
            print(f"ID: {folder['id']}, Name: {folder['name']}")
            return (folder['id'])
            
    else:
        print("Failed to retrieve folders.")
        print(f"Status Code: {response.status_code}")
        print(f"Response: {response.text}")
def upload_url_to_Fosology(folder_id,url):
    URLToSent=url
  
    data_raw={
               "location": {"url": URLToSent} ,
                "name":"test",
                "url": URLToSentl
                       
                }
    HEADERS1={
    "Authorization": f"Bearer {Fossology_API_TOKEN}",
    "Content-Type": "application/json",
    "folderId": '1',
    "uploadDescription": "test upload",
    "uploadType": "url"
}
    
    payload = {
     
        "location": {"url": URLToSent}  # Provide the location object
    }
    upload_url=f"{Fossology_API_URL}/uploads"
    
    response=requests.post(upload_url,json=payload,headers=HEADERS1)

    
    print(response.text)
if __name__ == "__main__":
    # upload_url_to_Fosology()
    folder_id=get_folders()
    upload_url_to_Fosology(folder_id)
    
   